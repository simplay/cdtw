from memory_profiler import profile
from collections import defaultdict
from scipy.sparse import csr_matrix
import numpy as np
import time


#@profile
def compute_ctw(f, g, w=1):
    # row = np.array([0, 0, 1, 2, 2, 2])
    # col = np.array([0, 2, 2, 0, 1, 2])
    # costs = np.array([1, 2, 3, 4, 5, 6])

    # distances squared - apply sqrt - de-spare-it
    D = defaultdict(lambda: defaultdict(lambda: np.Infinity))
    D[0][0] = 0.0

    n = len(f)
    m = len(g)

    w = np.max(w, abs(n - m))
    p = 1
    c = 2
    for idx in range(1, n + 1):
        # jdx_range = range(1, m + 1) # range of slow solution
        lower_bound = np.max([1, idx - w])
        upper_bound = np.min([m + 1, idx + w])
        jdx_range = range(lower_bound, upper_bound)
        for jdx in jdx_range:
            cost = (f[idx - 1] - g[jdx - 1]) ** 2

            # a = D[idx - 1][jdx]  # insertion
            # b = D[idx][jdx - 1]  # deletion
            # c = D[idx - 1][jdx - 1]  # match

            a = D[p][jdx]  # insertion
            b = D[c][jdx - 1]  # deletion
            c = D[p][jdx - 1]  # match

            dij = cost + np.min([a, b, c])
            D[c][jdx] = dij

        # swap
        tmp = p
        p = c
        c = tmp


    idx = n
    jdx = m
    final_indices = [(idx, jdx)]
    while (not idx == 0) and (not jdx == 0):
        a = D[idx - 1][jdx]  # insertion
        b = D[idx][jdx - 1]  # deletion
        c = D[idx - 1][jdx - 1]  # match

        index = np.argmin([a, b, c])

        if index == 0:
            idx -= 1
        elif index == 1:
            jdx -= 1
        elif index == 2:
            idx -= 1
            jdx -= 1

        final_indices.append((idx, jdx))

    return final_indices, D


def main(n: int, w: int):
    f = np.random.rand(n)
    g = np.random.rand(n)

    permutation, distances = compute_ctw(f, g, w)
    return permutation


if __name__ == '__main__':
    dt = time.time()
    main(n=10_000_000, w=1)

    print(time.time() - dt)
    print("done")
